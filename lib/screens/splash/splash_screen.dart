import 'dart:async';

import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  final StreamController<bool> webViewIsLoadedStream;
  final Function webViewDidLoadCallback;

  const SplashScreen({
    Key key,
    this.webViewIsLoadedStream,
    this.webViewDidLoadCallback,
  }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // MARK: - State
  bool _webViewIsLoaded = false;

  // MARK: - Life cycle
  void initState() {
    super.initState();
    setDidLoadWebViewListener();
  }

  // MARK: - Logic
  void setDidLoadWebViewListener() {
    widget.webViewIsLoadedStream.stream.listen((isLoad) {
      if (!_webViewIsLoaded && isLoad) {
        setState(() {
          _webViewIsLoaded = isLoad;
        });
      }
    });
  }

  // MARK: - UI
  Widget _buildScreen() {
      return Container(
        height: double.infinity,
        width: double.infinity,
        child: _buildContinueButton(),
      );
  }

  Widget _buildContinueButton() {
    if (_webViewIsLoaded) {
      return Center(
        child: InkWell(
          onTap: () {
            widget.webViewDidLoadCallback();
          },
          child: Container(
              height: 60,
              width: 200,
              child: Center(
                child: Text(
                  "Продолжить",
                  style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                  ),
                ),
              )),
        ),
      );
    } else {
      return Center(
        child: SizedBox(
          height: 60,
          width: 60,
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) => _buildScreen();
}
