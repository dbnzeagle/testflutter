import 'dart:async';

import 'package:flutter/material.dart';
import 'package:test_task/screens/splash/splash_screen.dart';

class SplashPage extends StatelessWidget {
  final StreamController<bool> webViewIsLoadedStream;
  final Function webViewDidLoadCallback;

  const SplashPage({
    Key key,
    this.webViewIsLoadedStream,
    this.webViewDidLoadCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      body: SplashScreen(
        webViewDidLoadCallback: webViewDidLoadCallback,
        webViewIsLoadedStream: webViewIsLoadedStream,
      ),
    );
  }
}