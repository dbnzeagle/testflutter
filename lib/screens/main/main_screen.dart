import 'package:flutter/material.dart';
import 'package:test_task/managers/device_info_manager.dart';
import 'package:test_task/managers/open_url_manager.dart';
import 'package:test_task/screens/Constants.dart';
import 'package:test_task/screens/page_display/display_deeplink_data_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:device_info/device_info.dart';

class MainScreen extends StatefulWidget {
  final Function webViewDidLoad;

  const MainScreen({
    Key key,
    this.webViewDidLoad,
  }) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  // MARK: - State
  WebViewController _defaultController;
  WebViewController _avieTicketsController;
  var androidDeviceInfo = DeviceInfoPlugin();
  int _currentIndexPage = 0;

  // MARK: - Life cycle
  void initState() {
    super.initState();
  }

  // MARK: - Logic
  void _handleFinishPage(String url) {
    widget.webViewDidLoad();
    print("_handleFinishPage");
    print("onPageFinished -> $url");
  }

  void _handlePageStarted(String url) {
    print(url);
    if (url.contains("russpass://")) {
      var urlData = url.replaceFirst("russpass://", "");
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (_) => DisplayDeepLinkDataScreen(
            urlData: urlData,
          ),
        ),
      );
    }
  }

  void _onPageTap(int index) {
    if (index == 0) {
      setState(() {
        _currentIndexPage = index;
      });
    } else {
      if (DeviceInfoManager.shared.isNeededBrowser) {
        OpenUrlManager.openUrl(Constants.secondPageUrl);
      } else {
        setState(() {
          _currentIndexPage = index;
        });
      }
    }

    print("_onPageTap $index");
  }

  // MARK: - UI
  Widget _buildScreen() {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: IndexedStack(
          index: _currentIndexPage,
          children: [
            _buildDefaultWebView(),
            _buildAviaTicketsWebView(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_view_day), title: Text('Feed')),
          BottomNavigationBarItem(
              icon: Icon(Icons.airplanemode_active),
              title: Text('Avia tickets')),
        ],
        currentIndex: _currentIndexPage,
        selectedItemColor: Colors.amber[800],
        onTap: _onPageTap,
      ),
    );
  }

  Widget _buildDefaultWebView() {
    return WebView(
      key: Key("default_web_view"),
      initialUrl: Constants.firstPageUrl,
      javascriptMode: JavascriptMode.unrestricted,
      onPageFinished: _handleFinishPage,
      onPageStarted: _handlePageStarted,
      onWebResourceError: (error) {
        print('ошибка');
        print(error.description);
      },
      onWebViewCreated: (webViewController) {
        _defaultController = webViewController;
      },
    );
  }

  Widget _buildAviaTicketsWebView() {
    if (DeviceInfoManager.shared.isNeededBrowser) {
      return Container(
        child: Center(
          child: Text("Did Open in Browser ))"),
        ),
      );
    }

    return WebView(
      key: Key("avia_tickets_web_view"),
      initialUrl: Constants.secondPageUrl,
      javascriptMode: JavascriptMode.unrestricted,
      onWebViewCreated: (webViewController) {
        _avieTicketsController = webViewController;
      },
    );
  }

  @override
  Widget build(BuildContext context) => _buildScreen();
}
