import 'package:flutter/material.dart';

class DisplayDeepLinkDataScreen extends StatelessWidget {
  final String urlData;

  const DisplayDeepLinkDataScreen({
    Key key,
    this.urlData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DeepLink"),
        brightness: Brightness.light,
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Данные из ссылки ПРОСТО"),
                Padding(padding: EdgeInsets.only(bottom: 16),),
                Text(urlData),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
