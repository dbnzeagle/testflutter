import 'dart:async';

import 'package:flutter/material.dart';
import 'package:test_task/managers/device_info_manager.dart';
import 'package:test_task/screens/main/main_screen.dart';
import 'package:test_task/screens/splash/splash_page.dart';
import 'package:device_info/device_info.dart';

class RootScreen extends StatefulWidget {
  @override
  _RootScreenState createState() => _RootScreenState();
}

class _RootScreenState extends State<RootScreen> {
  // MARK: - State
  int _currentIndex = 1;
  bool _webViewIsLoaded = false;
  bool _isLoading = true;
  var _webViewIsLoadStream = StreamController<bool>();

  void initState() {
    super.initState();
    _getDeviceInfo();
  }

  void dispose() {
    _webViewIsLoadStream.close();
    super.dispose();
  }

  // MARK: - Logic
  Future _getDeviceInfo() async {
    if (DeviceInfoManager.shared.isAndroid) {
      DeviceInfoManager.shared.androidDeviceInfo = await DeviceInfoPlugin().androidInfo;
    }

    setState(() {
      _isLoading = false;
    });
  }

  void _webViewDidLoad() {
    if (!_webViewIsLoaded) {
      setState(() {
        _webViewIsLoaded = true;
        _webViewIsLoadStream.add(true);
        print("_webViewDidLoad");
      });
    }
  }

  void _webViewDidLoadCallback() {
    setState(() {
      _currentIndex = 0;
      print("_webViewDidLoadCallback");
    });
  }

  // MARK: - UI
  Widget _buildScreen() {
    if (_isLoading) {
      return Container(
        color: Colors.green,
      );
    } else {
      return IndexedStack(
        index: _currentIndex,
        children: [
          MainScreen(
            webViewDidLoad: _webViewDidLoad,
          ),
          SplashPage(
            webViewIsLoadedStream: _webViewIsLoadStream,
            webViewDidLoadCallback: _webViewDidLoadCallback,
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) => _buildScreen();
}
