import 'package:url_launcher/url_launcher.dart';

class OpenUrlManager {
  static Future openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}