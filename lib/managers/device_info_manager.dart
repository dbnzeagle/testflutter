import 'dart:io';

import 'package:device_info/device_info.dart';

class DeviceInfoManager {

  static var shared = DeviceInfoManager();

  AndroidDeviceInfo androidDeviceInfo;
  bool isAndroid = Platform.isAndroid;
  bool get isNeededBrowser {
    return isAndroid && androidDeviceInfo.version.sdkInt < 28;
  }
}