import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_task/screens/root/root_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
      ),
    );
    return MaterialApp(
      title: 'RussPass',
      home: RootScreen(),
    );
  }
}